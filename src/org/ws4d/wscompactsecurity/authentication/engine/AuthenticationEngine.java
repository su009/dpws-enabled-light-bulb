package org.ws4d.wscompactsecurity.authentication.engine;

import java.util.Vector;

import org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper;

//TODO!!!!!!!!
//FIXME!!!!!!!
//
// Right now, only a single connection can be covered at a time. No good.

public class AuthenticationEngine {
	
	private static AuthenticationEngine instance = null;
	
	private ECC_DH_Helper mCryptoHelper = null;
	
	private class SecurityAssociation {
		private String mID;
		private byte[] mMasterKey;
		public SecurityAssociation(String id, byte[] key) {
			setID(id);
			setMasterKey(key);
		}
		public byte[] getMasterKey() {
			return mMasterKey;
		}
		public void setMasterKey(byte[] masterKey) {
			this.mMasterKey = masterKey;
		}
		public String getID() {
			return mID;
		}
		public void setID(String mID) {
			this.mID = mID;
		}
	}
	
	private Vector<SecurityAssociation> securityAssociations;
	
	public static AuthenticationEngine getInstance() {
		if (instance == null)
			instance = new AuthenticationEngine();
		return instance;
	}
	
	private AuthenticationEngine() {
		setCryptoHelper(new ECC_DH_Helper());
		this.securityAssociations = new Vector<SecurityAssociation>();
	}
	
	public boolean removeSecurityAssociation(String id) {
		for (SecurityAssociation sa : this.securityAssociations) {
			if (sa.getID().equalsIgnoreCase(id)) {
				return this.securityAssociations.remove(sa);
			}
		}
		return false;
	}
	
	public void removeAllSecurityAssociations() {
		this.securityAssociations.clear();
	}
	
	public void updateSecurityAssociation(String id, byte[] key) {
		for (SecurityAssociation s : this.securityAssociations) {
			if (s.getID().equals(id)) {
				s.setMasterKey(key);
				return;
			}
		}
		this.securityAssociations.add(new SecurityAssociation(id, key));
	}
	
	public byte[] keyById (String id) {
		for (SecurityAssociation s : securityAssociations) {
			if (s.getID().equals(id)) {
				return s.getMasterKey();
			}
		}
		return null;
	}

	public ECC_DH_Helper getCryptoHelper() {
		return mCryptoHelper;
	}

	public void setCryptoHelper(ECC_DH_Helper cryptoHelper) {
		this.mCryptoHelper = cryptoHelper;
	}
	
	public void reset() {
		this.mCryptoHelper = null;
		setCryptoHelper(new ECC_DH_Helper());
	}
}
