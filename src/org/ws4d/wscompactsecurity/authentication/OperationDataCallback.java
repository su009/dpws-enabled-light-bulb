package org.ws4d.wscompactsecurity.authentication;


public interface OperationDataCallback {
	public String testOperationSetData(CallbackDataObject data);
}
