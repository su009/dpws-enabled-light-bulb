package org.ws4d.wscompactsecurity.authentication.eccdh;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.Constants;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityTokenResponse;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityTokenResponseCollection;
import org.ws4d.wscompactsecurity.authentication.engine.AuthenticationEngine;

public class ECC_DH2 extends Operation {

	OperationDataCallback cb = null;
	
	public ECC_DH2() {
		this(null);
	}
	
	public ECC_DH2(OperationDataCallback cb) {
		super("ECC_DH2", new QName(Constants.AuthenticationServiceECCDHPortType, Constants.Namespace));
		this.cb = cb;
		
		Element in = new Element(new QName("RequestSecurityTokenResponse", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponse());
		Element out = new Element(new QName("RequestSecurityTokenResponseCollection", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponseCollection());
		
		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org"), SchemaUtil.TYPE_STRING));
		this.addFault(f1);
		
		this.setInput(in);
		this.setOutput(out);
		
	}
	
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		Log.debug(" ---------------- Invoked ECC_DH2 ---------------- ");
		
		AuthenticationEngine engine = AuthenticationEngine.getInstance();
		
		if (engine == null) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error! No Authentication Engine Instance!");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}
		
		byte[] encodedPublicKey = Base64Util.decode(ParameterValueManagement.getString(parameterValue, "RequestedSecurityToken/auth-ecc-dh-public-key"));
		engine.getCryptoHelper().getParams().setForeignPublicKey(ECC_DH_Helper.decodePublicKey(encodedPublicKey));
		
		engine.getCryptoHelper().getParams().setForeignCMAC(Base64Util.decode(ParameterValueManagement.getString(parameterValue, "RequestedSecurityToken/auth-ecc-dh-cmac")));
		
		engine.getCryptoHelper().getParams().setNonceB(Base64Util.decode(ParameterValueManagement.getString(parameterValue, "RequestedSecurityToken/auth-ecc-dh-nonce")));
		
		try {
			engine.getCryptoHelper().calculateSharedSecret();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchProviderException e) {
			e.printStackTrace();
		}
		
		engine.getCryptoHelper().calculateCMAC();
		
		if (engine.getCryptoHelper().checkCMAC()) {
			Log.debug("Alice's CMAC passed the test! All fine");
		} else {
			Log.error("Alice's CMAC did NOT pass the test! No Good!!");

			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Authentication Error! CMAC did not match!");
			engine.reset();
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}
		
		engine.getCryptoHelper().calculateMasterKey();
		
		System.out.println("Calculated Master Key:");
		byte[] k = engine.getCryptoHelper().getParams().getMasterKey();
		for (int i = 0; i < k.length; i++) {
			System.out.printf(" %02x", k[i]);
		}
		System.out.println("\n\n");
		
		ParameterValue result = createOutputValue();
		ParameterValueManagement.setString(result, "RequestSecurityTokenResponse/TokenType", "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
		ParameterValueManagement.setString(result, "RequestSecurityTokenResponse/RequestType", "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH2Response");
		ParameterValueManagement.setString(result, "RequestSecurityTokenResponse/RequestedSecurityToken/auth-ecc-dh-cmac", Base64Util.encodeBytes(engine.getCryptoHelper().getParams().getCMAC()));
		
		engine.updateSecurityAssociation(engine.getCryptoHelper().getParams().getTarget(), engine.getCryptoHelper().getParams().getMasterKey());
		engine.reset();
		
		return result;
	}

}
