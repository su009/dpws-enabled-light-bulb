package org.ws4d.wscompactsecurity.authentication.eccdh;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.examples.sibi.tutorial.device.BulbHandler;
import org.ws4d.java.schema.Element;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.mobile.authentication.flicker.BitKeyTool;
import org.ws4d.mobile.authentication.flicker.FramboosKeyOutputDriver;
import org.ws4d.wscompactsecurity.authentication.Constants;
import org.ws4d.wscompactsecurity.authentication.OperationDataCallback;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityToken;
import org.ws4d.wscompactsecurity.authentication.eccdh.types.RequestSecurityTokenResponse;
import org.ws4d.wscompactsecurity.authentication.engine.AuthenticationEngine;

import framboos.OutPin;

public class ECC_DH1 extends Operation {

	OperationDataCallback cb = null;
	private OutPin mOutPin = null;
	
	public ECC_DH1() {
		this(null);
	}
	
	public ECC_DH1(OperationDataCallback cb) {
		super("ECC_DH1", new QName(Constants.AuthenticationServiceECCDHPortType, Constants.Namespace));
		this.cb = cb;
		
		BulbHandler h = BulbHandler.getInstance();
		this.mOutPin = (h==null) ? null : h.getBulbPin();
		
		Element in = new Element(new QName("RequestSecurityToken", Constants.Namespace_WS_Trust), new RequestSecurityToken());
		Element out = new Element(new QName("RequestSecurityTokenResponse", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponse());
		
		this.setInput(in);
		this.setOutput(out);
		
	}
	
	private void populateOOBSharedSecret() {
		// TODO: Some magic to happen here...
		AuthenticationEngine e = AuthenticationEngine.getInstance();
		System.out.println(" =================================================== ");
		System.out.println(" ===    PIN: " + e.getCryptoHelper().getParams().getOOBSharedSecretAsInt());
		System.out.println(" =================================================== ");
		
		BitKeyTool bkt = new BitKeyTool(new FramboosKeyOutputDriver(this.mOutPin));
		bkt.getDriver().setSymbolWidth(500);
		bkt.intToBool(e.getCryptoHelper().getParams().getOOBSharedSecretAsInt());
		bkt.driveKeyOut();
	}

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		Log.debug(" ---------------- Invoked ECC_DH1 ---------------- ");
		
		ParameterValue result = createOutputValue();
		
		AuthenticationEngine engine = AuthenticationEngine.getInstance();
		try {
			engine.getCryptoHelper().init();
			engine.getCryptoHelper().getParams().setOrigin(ParameterValueManagement.getString(parameterValue, "AppliesTo"));
			engine.getCryptoHelper().getParams().setTarget(ParameterValueManagement.getString(parameterValue, "OnBehalfOf"));
			
			String authType = ParameterValueManagement.getString(parameterValue, "AuthenticationType");
			
			if (!authType.equals("http://www.ws4d.org/authentication/ui/mechanisms#flicker")) {
				/* 
				 * Do whatever you like to indicate a fatal error - authentication is not possible
				 * 
				 * However, once, the is set active, consider indirect authentication via STS
				 */
				Log.error("FATAL ERROR!! AuthenticationType " + authType +" not supported!");
			}
			
			engine.getCryptoHelper().getParams().addOtherA(authType);
			engine.getCryptoHelper().getParams().addOtherB(authType);
			
			Random r = new Random(System.currentTimeMillis()); /* some seed - maybe use something more secure... */
			boolean[] oobSharedSecret = new boolean[20];
			for (int i=0; i<20; i++) {
				oobSharedSecret[i] = r.nextBoolean();
			}
			
			engine.getCryptoHelper().getParams().setOOBSharedSecret(oobSharedSecret);
			engine.getCryptoHelper().OOBkey2Integer();
			
			engine.getCryptoHelper().createNonce();
			engine.getCryptoHelper().encryptPublicKey();
			
			ParameterValueManagement.setString(result, "TokenType", "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
			ParameterValueManagement.setString(result, "RequestType", "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH1Response");
			ParameterValueManagement.setString(result, "AppliesTo", engine.getCryptoHelper().getParams().getTarget());
			ParameterValueManagement.setString(result, "OnBehalfOf", engine.getCryptoHelper().getParams().getOrigin());
			ParameterValueManagement.setString(result, "AuthenticationType", "http://www.ws4d.org/authentication/ui/mechanisms#flicker");
			
			ParameterValueManagement.setString(result, "RequestedSecurityToken/auth-ecc-dh-curvename", engine.getCryptoHelper().getParams().getCurveName());
			ParameterValueManagement.setString(result, "RequestedSecurityToken/auth-ecc-dh-nonce", Base64Util.encodeBytes(engine.getCryptoHelper().getParams().getNonceA()));
//			ParameterValueManagement.setAttributeValue(result, "RequestedSecurityToken/auth-ecc-dh-nonce", "encoding", "base64");
			ParameterValueManagement.setString(result, "RequestedSecurityToken/auth-ecc-dh-public-key", Base64Util.encodeBytes(engine.getCryptoHelper().getParams().getDistortedPublicKey().getEncoded()));
//			ParameterValueManagement.setAttributeValue(result, "RequestedSecurityToken/auth-ecc-dh-public-key", "encoding", "base64");
			//FIXME: Setting Attributes results in error...
			
			
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		populateOOBSharedSecret();
		return result;
	}
	
	

}
