package org.ws4d.wscompactsecurity.authentication.eccdh.types;

import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;

public class PublicKey extends ComplexType {

	public PublicKey() {
		super(new QName("auth-ecc-dh-public-key", Constants.Namespace), ComplexType.CONTAINER_SEQUENCE);
		Element xElement = new Element(new QName("auth-ecc-dh-public-key-X", Constants.Namespace), SchemaUtil.TYPE_STRING);
		Element yElement = new Element(new QName("auth-ecc-dh-public-key-Y", Constants.Namespace), SchemaUtil.TYPE_STRING);
		this.addElement(xElement);
		this.addElement(yElement);
	}

}
