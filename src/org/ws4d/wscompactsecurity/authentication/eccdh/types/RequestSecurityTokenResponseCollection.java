package org.ws4d.wscompactsecurity.authentication.eccdh.types;

import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.Constants;

public class RequestSecurityTokenResponseCollection extends ComplexType {

	public RequestSecurityTokenResponseCollection() {
		super(new QName("RequestSecurityTokenResponseCollection", Constants.Namespace_WS_Trust), ComplexType.CONTAINER_SEQUENCE);
		
		Element e = new Element(new QName("RequestSecurityTokenResponse", Constants.Namespace_WS_Trust), new RequestSecurityTokenResponse());
		
		this.addElement(e);
		
	}

}
