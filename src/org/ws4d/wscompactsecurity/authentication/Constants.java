package org.ws4d.wscompactsecurity.authentication;

public class Constants {

	public static String Namespace = "http://www.ws4d.org";
	public static String Namespace_WS_Security = Namespace;//"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	public static String Namespace_WS_Trust = Namespace;//"http://docs.oasis-open.org/ws-sx/ws-trust/200512";
	public static String Namespace_WS_Policy = Namespace;//"http://schemas.xmlsoap.org/ws/2004/09/policy";
	public static String Namespace_WS_Security_Utility = Namespace;//"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	public static String DevicePortTypeUIAuthenticator = "UI-Authenticator";
	public static String DevicePortTypeAuthenticator = "Authenticator";
	public static String AuthenticationServiceID = Constants.Namespace + "/AuthenticationService"; 
	public static String AuthenticationServiceTestPortType = "SimpleTestOperations";
	public static String AuthenticationServiceECCDHPortType= "AuthenticatedEllipticCurveDiffieHellman";
}
