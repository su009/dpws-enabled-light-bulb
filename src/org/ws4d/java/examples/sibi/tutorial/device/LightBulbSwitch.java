package org.ws4d.java.examples.sibi.tutorial.device;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.wscompactsecurity.authentication.engine.AuthenticationEngine;

import framboos.OutPin;

public class LightBulbSwitch extends Operation{

	private boolean error = false; 
	
	public LightBulbSwitch() {
		super("LightBulbSwitch", new QName("LightBulbService", "http://www.ws4d.org/"));
		
		Type xsString = SchemaUtil.TYPE_STRING;
		
		ComplexType input = new ComplexType(ComplexType.CONTAINER_ALL);
				
		Element stateElem = new Element(new QName("state", "http://www.ws4d.org/"), xsString);
		Element idElem = new Element(new QName("id", "http://www.ws4d.org/"), xsString);
		
		input.addElement(stateElem);
		input.addElement(idElem);
		
		Fault f1 = new Fault("SimpleError");
		f1.setElement(new Element(new QName("message", "http://www.ws4d.org/"), xsString));
		this.addFault(f1);
						
		setInput(new Element(input));
		setOutput(stateElem);
	}
	

	
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		
		String newState = ParameterValueManagement.getString(parameterValue, "state");

		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		
		if (ae == null) {
			ParameterValue fault = createFaultValue("SimpleError");
			ParameterValueManagement.setString(fault, "message", "Internal Server Error!");
			throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
		}
		
		String rId = ParameterValueManagement.getString(parameterValue, "id");
		
		if (rId == null || ae.keyById(rId) == null) { /* no id specified or no key for id... */
			if (!ButtonHandler.isPinPressed()) { /* ... unless the button is pressed */
				ParameterValue fault = createFaultValue("SimpleError");
				ParameterValueManagement.setString(fault, "message", "Client was not authenticated!");
				throw new InvocationException(getFault("SimpleError"), new QName("simple", "http://www.ws4d.org"),  fault);
			}
		}		
		
		ParameterValue result = createOutputValue();
		
		BulbHandler h = BulbHandler.getInstance();
		
		if (h == null)
			this.error = true;
		else {
			OutPin bulbPin = h.getBulbPin();
			if (newState.toLowerCase().equals("off")) {
				h.setBulbOn(false);
				if (bulbPin != null) {
					bulbPin.setValue(false);
				}
				this.error = false;
			} else if (newState.toLowerCase().equals("on")) {
				h.setBulbOn(true);
				if (bulbPin != null) {
					bulbPin.setValue(true);
				}
				this.error = false;
			} else {
				this.error = true;
			}
		}
		
		if (this.error) {
			ParameterValueManagement.setString(result, "state", "--error in request--");
			
		} else {
			ParameterValueManagement.setString(result, "state", h.getStateAsString());
			System.out.println("Will turn bulb " + h.getStateAsString());
		}
		
		return result;
	}
	
}
