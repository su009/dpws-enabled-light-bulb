package org.ws4d.java.examples.sibi.tutorial.device;

import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;

import framboos.OutPin;

public class LightBulbExampleDevice extends DefaultDevice {
	
	@Override
	protected void deviceIsStopping() {
		BulbHandler h = BulbHandler.getInstance();
		OutPin pin = (h==null) ? null : h.getBulbPin();
		if (pin != null)
			pin.close();
	}

	public LightBulbExampleDevice() {
		super();
		/* in case pin is still open, force close it before reopening */
		this.setFirmwareVersion("5");
		
		this.setPortTypes(new QNameSet(new QName("LightbulbDevice", "http://www.ws4d.org/")));
		
		this.addFriendlyName("en-US", "MyShisselBulb");
		this.addFriendlyName(LocalizedString.LANGUAGE_DE, "MeineKrasseGluehbirne");
		
		this.addManufacturer(LocalizedString.LANGUAGE_EN, "Light Bulbs Inc.");
		this.addManufacturer(LocalizedString.LANGUAGE_DE, "Gluehbirnen GmbH");
		
		this.addModelName(LocalizedString.LANGUAGE_DE, "12345");
		this.addModelName(LocalizedString.LANGUAGE_EN, "12345");
		
		this.setEndpointReference(new EndpointReference(new URI("urn:uuid:19fc6dc0-e254-11e2-80c3-01cd7c7b068b")));
		
	}
	
	
	
}
