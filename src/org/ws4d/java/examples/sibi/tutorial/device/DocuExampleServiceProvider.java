package org.ws4d.java.examples.sibi.tutorial.device;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.wscompactsecurity.authentication.AuthenticationService;

public class DocuExampleServiceProvider {
	
	public static void main(String[] args) {
		BulbHandler.init(0);
		ButtonHandler.init(1);
		
		System.out.println("Test: Right now, Button 18 is " + ButtonHandler.isPinPressed());
		
		JMEDSFramework.start(args);
		LightBulbExampleDevice device = new LightBulbExampleDevice();
		final LightBulbService service = new LightBulbService();

		service.addOperation(new LightBulbState());
		
		//FIXME: Don't hand in PIN (and maybe state) as parameters but maybe use global classes...
		service.addOperation(new LightBulbSwitch());
		
		device.addService(service);
		
		device.addService(new AuthenticationService());
		
		try {
			device.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
