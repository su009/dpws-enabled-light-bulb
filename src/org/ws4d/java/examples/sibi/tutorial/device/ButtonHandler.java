package org.ws4d.java.examples.sibi.tutorial.device;

import org.ws4d.wscompactsecurity.authentication.engine.AuthenticationEngine;

import framboos.InPin;
import framboos.util.HardReset;

public class ButtonHandler {
	private ButtonHandler() {
		this(1);
	}
	
	private static InPin mInPin = null;
	private PinWatcher mPinWatcher = null;
	
	private class PinWatcher extends Thread {

		private int mCheckInterval;
		private int mTimeOut;

		private long timeStamp=0;
		
		public PinWatcher(int timeOut, int checkInterval) {
			this.mTimeOut = timeOut;
			this.mCheckInterval = checkInterval;
		}

		@Override
		public void run() {
			while (true) {
				if (mInPin.getValue()) {
					if (timeStamp == 0) {
						timeStamp = System.currentTimeMillis();
					} else {
						if ((System.currentTimeMillis() - timeStamp) > mTimeOut) {
							timeStamp = 0;
							// TODO: work with callback interface (for a reason I don't know yet...)
							AuthenticationEngine ae = AuthenticationEngine.getInstance();
							if (ae != null) {
								ae.removeAllSecurityAssociations();
							}
							System.err.println("Cleared Security Associations!");
						}
					}
				} else {
					timeStamp = 0;
				}
				try {
					Thread.sleep(mCheckInterval);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static ButtonHandler instance = null;
	
	public static ButtonHandler getInstance() {
		return instance;
	}
	
	public static void init(int i) {
		if (instance == null) {
			instance = new ButtonHandler(i);
		}
	}
	
	private ButtonHandler(int pinNumber) {
		/* in case pin is still open, force close it before reopening */
		HardReset.hardResetPin(pinNumber);
		mInPin = new InPin(pinNumber);
		mPinWatcher = new PinWatcher(5000, 250);
		mPinWatcher.start();
	}
	
	public static Boolean isPinPressed() {
		if (mInPin == null)
			return null;
		return Boolean.valueOf(mInPin.getValue());
	}
	
}
