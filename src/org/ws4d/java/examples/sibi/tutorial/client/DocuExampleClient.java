package org.ws4d.java.examples.sibi.tutorial.client;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.protocol.http.HTTPBinding;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.types.URI;

public class DocuExampleClient extends DefaultClient {

	/**
	 * @param args
	 */
	
	private ClientSubscription notificationSub = null;
	private final static String namespace = "http://sibi.org/";
	
	public static void main(String[] args) {
		JMEDSFramework.start(args);
		DocuExampleClient client = new DocuExampleClient();
		
		((IPAddress)IPNetworkDetection.getInstance().getAddresses("inet4", null).next()).getAddress();
		
		System.out.println( ((IPAddress)IPNetworkDetection.getInstance().getAddresses("inet4", null).next()).getAddress() );
		
		client.registerHelloListening();
		
		SearchParameter deviceSearch = new SearchParameter();
		SearchParameter serviceSearch = new SearchParameter();
		
		deviceSearch.setDeviceTypes(new QNameSet(new QName("LightbulbExampleDevice", namespace)));
		serviceSearch.setServiceTypes(new QNameSet(new QName("BasicServices", namespace)));
		
		SearchManager.searchDevice(deviceSearch, client, null);
		SearchManager.searchService(serviceSearch, client);
		
	}
	
	@Override
	public void helloReceived(HelloData helloData) {
		try {
			Device device = getDeviceReference(helloData).getDevice();
			System.out.println("Received Hello from: ");
			
			System.out.println("Endpoint Reference\t" + (device.getEndpointReference() != null ? device.getEndpointReference().getAddress().toString() : null));
			
		} catch (CommunicationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search) {
		System.out.println("Found a Device!");
		
		try {
			ServiceReference servRef = (ServiceReference) devRef.getDevice().getServiceReferences(devRef.getSecurityKey()).next();
			EventSource eventSource = servRef.getService().getAnyEventSource(new QName("BasicServices", namespace), "DocuExampleSimpleEvent");
			
			if (eventSource == null)
				System.out.println("COULD FIND NO EVENT SOURCE!");
			else {
				DataStructure bindings = new org.ws4d.java.structures.ArrayList();
				HTTPBinding binding = new HTTPBinding((IPAddress)IPNetworkDetection.getInstance().getAddresses("inet4", null).next(), 10235, "/EventSink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
				bindings.add(binding);

				// subscribe
				notificationSub = eventSource.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			}
		} catch (EventingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}

	@Override
	public void serviceFound(ServiceReference servRef, SearchParameter search) {
		System.out.println("Found a Service!");
		try {
			Operation op = servRef.getService().getAnyOperation(new QName("BasicServices", "http://sibi.org/"), "LightBulbSwitchOnOffOperation");
			
			/* switch bulb on, set to 85% */
			ParameterValue request = op.createInputValue();
			ParameterValueManagement.setString(request, "state", "on");
			ParameterValueManagement.setString(request, "dim_state", "85");
			ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			
			System.out.println("Bulb switched " + ParameterValueManagement.getString(result, "state") + " and set to " + ParameterValueManagement.getString(result, "dim_state") + "%");
			
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public ParameterValue eventReceived(ClientSubscription subscription,
			URI actionURI, ParameterValue parameterValue) {
		
		if (subscription.equals(notificationSub)) {
			System.out.println("Received Event: " + ParameterValueManagement.getString(parameterValue, "name"));
		}
		
		return null;
	}

	@Override
	public void subscriptionEndReceived(ClientSubscription subscription,
			int reason) {
		// TODO Auto-generated method stub
		System.out.println("Subscription Ended");
	}

	@Override
	public void subscriptionTimeoutReceived(ClientSubscription subscription) {
		// TODO Auto-generated method stub
		subscriptionEndReceived(subscription, SubscriptionEndMessage.WSE_STATUS_UNKNOWN);
	}
	
	

	
	
}
